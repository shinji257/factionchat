This is a fork of the official FactionChat repositiory available here: https://github.com/James137137/FactionChat

All original code is attributed to the original author.  I take no claim to it and only to the modifications that have been logged on this fork.  Even then that's really a gray area...

All modifications are in the Tekkit Branch (which should be default anyways)

Changes:
Replaced log.log functions with log.info for console
Replaced AsyncPlayerChatEvent with PlayerChatEvent (Async version unavailable for Tekkit Classic)
Removed the updater and the metrics since this is an unofficial fork and I didn't feel like fixing metrics (version it uses doesn't work on TC)
Update option in game does nothing now

Building:
Use Maven to build and package.  pom.xml file is included.